/**
 * It's recommended to pass the requests events to Analytics
 * before response to user and after response with errors,
 * For example we can send some events to Amplitude Analytics
 * to store as many user props as possible and use them
 * in targetting, (eg target user X with news about La Liga) as long
 * as he's visiting La Liga results a lot.
 * Also we should send all failure (throw err, reject promise) to
 * Amplitude (or any other tool), so we know where do we have failure.
 * That's a nutshell about how can we use events for this Test.
 * Note that: All events has props, so for the same error event
 * We can have a variant causes (Db error, http error, ...)
 * I've called tracker in 2 positions just for example of use
 * it's console.log ATM.
 * Another powerful tool we can use Bugsnag for reporting and logging.
 */

const express = require('express');
const mysql_pool = require('../services/mysql').mysql_pool;
const firewall = require('../services/firewall');
const helper = require('../helper/season-helper');
const translator = require('../helper/translator');
const tracker = require('../events');
const config = require('../config');
const logger = require('../logger');
const router = express.Router();


// Number of results per page/request.
const pageSize = 20;

let routeRedis = null;

const sortingEnum = Object.freeze({
    DATE: "Date",
    HOMETEAM: "HomeTeam",
    AWAYTEAM: "AwayTeam",
    DIV: "Div",
    ID: "ID"
});
const sortingTypeEnum = Object.freeze({
    ASC: "asc",
    DESC: "desc"
});
function isInt(value) {
    return !isNaN(value) && 
           parseInt(Number(value)) == value && 
           !isNaN(parseInt(value, 10));
}

router.get("/list", (req, resp) => {
    firewall.check(req.header('graceToken')).then(() => {
        getList(req.get('host')).then(res => {
            resp.send(res);
        }).catch(() => {
            logger.error("Http error", "Error getting list");
            resp.status(500).end();
        });
    }).catch(() => {
        tracker({
            event_name: "http-events",
            event_success: false,
            event_cause: "Access denied, ApiKey not valid",
            event_props: [req.header('graceToken')]
        });
        logger.error("Http error", "Access denied");
        resp.status(403).end();
    });
    
});

/**
 * 
 * @param {host eg domain name} params 
 * @returns season and league pairs
 */
function getList(host) {
    
    return new Promise(function(res) {
        routeRedis(config.use_redis).then(redis => {
            redis.hgetall("leagueList0").then((data) => {
                res(JSON.parse(data['data']));
            }).catch(() => {
                getListFromDB(host).then(results => {
                    if (config.use_redis) {
                        passToRedis(redis, "leagueList0", results);
                    }
                    res(results);
                })
            });
        }).catch(() => {
            getListFromDB(host).then(results => {
                res(results);
            })
        });
    });
}

/**
 * 
 * @param {Redis object} redis 
 * @param {Key for redis hash} key 
 * @param {data to be passed} data 
 * @param {seconds to keep this hash in memory} expire 
 */
function passToRedis(redis, key, data, expire=86400) {
    redis.hmset(key, {data: JSON.stringify(data)});
    redis.expire(key, expire);
}

function getListFromDB(host) {
    var results = [];
    var query = `SELECT Season, \`Div\` as League FROM \
    data group by Season, League`;
    return new Promise (function(res) {
        mysql_pool.getConnection(function (err, connection) {
        if (err) throw err;
        connection.query(query ,[], function (err, rows) {
            if (err) {
                throw err;
            } 
            rows.map(e => {
                let season = e.Season.toString();
                let finalSeason = helper.readable(season);
                let url = `http://${host}/league?`
                results.push({
                    "title": translator(e.League) + " " + finalSeason,
                    results: url+`season=${season}&div=`+e.League
                }); 
            });
            res(results);
            connection.release();
        });
    });
});
}

router.get('/league', (req, resp) => {
    firewall.check(req.header('graceToken')).then(() => {
        getLeagueResults(req.query.season, req.query.div, req.query.sort, req.query.sorttype, req.query.offset).then(res => {
            resp.send(res);
        }).catch(() => {
            logger.error("Http error", "Error getting results");
            resp.status(500).end();
        });
    }).catch(() => {
        logger.error("Http error", "Access denied");
        tracker({
            event_name: "http-events",
            event_success: false,
            event_cause: "Access denied, ApiKey not valid",
            event_props: [req.header('graceToken')]
        });
        resp.status(403).end();
    });
});

/**
 * 
 * @param {Season in DB, for example 201515} season 
 * @param {League} _div
 * @param {Order by} sort 
 * @param {Asc, Desc} sortType 
 * @returns Promise with rows.
 */
function getLeagueResults(season, _div, sort, sortType, offset=0) {
    return new Promise(function(res) {
        var mysort = "Date";
        if (Object.values(sortingEnum).includes(sort)) {
            mysort = sort;
        }
        var mysortType = "desc";
        if (Object.values(sortingTypeEnum).includes(sortType)) {
            mysortType = sortType;
        }
        // Let's get the count of records so we can offset some results if needed.
        let countQuery = "SELECT count(*) from data WHERE Season like ? and `Div` like ?";
        getCount(countQuery, [season, _div]).then(counter => {
            let myoffset = isInt(offset) ? Math.abs(offset) : 0;
            let query = `SELECT * FROM data WHERE Season like ? and \`Div\` like ? order by \`${mysort}\` ${mysortType} limit ${myoffset},${pageSize}`;
            mysql_pool.getConnection(function (err, connection) {
                if (err) throw err;
                connection.query(query, [season, _div], function(err, rows) {
                    if (err) throw err;
                    tracker({
                        event_name: "mysql-events",
                        event_success: true,
                        event_cause: "Http request from user, getting League results",
                        event_props: [season, _div]
                    });
                    res({results: rows,resultsPerRequest: pageSize, totalCount: counter['count(*)']});
                    connection.release();
                });
            });
        });
    });
}

/**
 * 
 * @param {The query needs to be passed to get the count(*)} query 
 * @param {array of fields that replace the ? in the query} fields 
 * @returns Promise with count(*)
 */
function getCount(query, fields) {
    return new Promise(function(res) {
        mysql_pool.getConnection(function (err, connection) {
            if (err) throw err;
            connection.query(query, fields, function(err, rows) {
                if (err) {
                    tracker({
                        event_name: "mysql-events",
                        event_success: false,
                        event_cause: "error " + err.message,
                        event_query: query,
                        event_fields: fields
                    });
                    throw err;
                }
                res(rows[0]);
                connection.release();
            });
        });
    });
}

module.exports = function(redis) {
    routeRedis = redis;
    return router;
}
