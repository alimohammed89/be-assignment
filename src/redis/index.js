const config = require('../config');
const Redis = require("ioredis");
const logger = require('../logger');
const redis = new Redis({
    host: config.redis_host,
    connectTimeout: 500,
    retryStrategy: function (times) {
        if (times > 3) {
          logger.error("redisRetryError", 'Redis reconnect exhausted after 3 retries.');
          return null;
        }
        return 200;
      }
});
  
module.exports = function(useRedis) {
    return new Promise (function(res, rej) {
        if (! useRedis || redis.status != "ready"){
            rej();
        }
        else {
            res(redis);
        }
    });
}
