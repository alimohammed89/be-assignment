const chai = require('chai');
const expect = chai.expect;
const sinon = require('sinon');
const http = require('http');

const someRequest = require('./some-requests');

describe('Requests to our server', function() {
    let httpGetStub;
    beforeEach(function() {
        // We want to stub all http connections.
        httpGetStub = sinon.stub(http, 'get'); 
    });
    afterEach(function() {
        httpGetStub.restore();
    });

    it('Responses with success message', function() {
        httpGetStub.yields({ headers: { 'content-type': 200 }});
        return someRequest.normal.then(res => { 
            expect(res).to.equal('Connteced to server');      
        });
    });

    it('Rejects with error message', function() {
        httpGetStub.yields({ headers: { 'content-type': 400 }});
        return someRequest.normal.catch(err => { 
            expect(err.message).to.equal('Failed to connect to server');      
        });
    });

    it('List response - payload exists', function() {
        httpGetStub.yields({ headers: { 'content-type': 200 }});
        return someRequest.list.then(res => { 
            expect(res).to.have.lengthOf(2); 
        });
    });

    it('List response - empty list', function() {
        httpGetStub.yields({ headers: { 'content-type': 400 }});
        return someRequest.list.catch(err => { 
            expect(err.message).to.have.lengthOf(0);    
        });
    });

    it('Details response - payload exists', function() {
        httpGetStub.yields({ headers: { 'content-type': 200 }});
        return someRequest.details.then(res => { 
            expect(res.results).to.have.lengthOf(2); 
        });
    });

    it('Details response - empty list', function() {
        httpGetStub.yields({ headers: { 'content-type': 400 }});
        return someRequest.details.catch(err => { 
            expect(err.message.results).to.have.lengthOf(0);    
        });
    });
});
