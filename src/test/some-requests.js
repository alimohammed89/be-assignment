const randomRequests = {
    normal: new Promise(function(res,rej) {
        res('Connteced to server');
        rej('Failed to connect to server');
    }),
    list: new Promise(function(res,rej) {
        let sample = [
            {
                title: "La Liga Primera 2015-2016",
                results: "http://localhost:3000/league?season=201516&div=SP1"
            },
            {
                league: "La Liga Segunda 2015-2016",
                season: "http://localhost:3000/league?season=201516&div=SP2"
            }
        ];
        res(sample);
        rej([]);
    }),
    details: new Promise(function(res,rej) {
        let sample = {
            results: [
                {
                    ID: 771,
                    Div: "SP2",
                    Season: 201617,
                    Date: "2016-08-21T21:00:00.000Z",
                    HomeTeam: "Zaragoza",
                    AwayTeam: "UCAM Murcia",
                    FTHG: 3,
                    FTAG: 1,
                    FTR: "H",
                    HTHG: 3,
                    HTAG: 0,
                    HTR: "H\r"
                },
                {
                    ID: 793,
                    Div: "SP2",
                    Season: 201617,
                    Date: "2016-09-03T21:00:00.000Z",
                    HomeTeam: "Zaragoza",
                    AwayTeam: "Huesca",
                    FTHG: 1,
                    FTAG: 0,
                    FTR: "H",
                    HTHG: 0,
                    HTAG: 0,
                    HTR: "D\r"
                }]
        };
        res(sample);
        rej({results: []});
    }), 
};

module.exports = randomRequests;
