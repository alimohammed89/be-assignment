const winston = require('winston');
const logger = winston.createLogger({
    transports: [
        new winston.transports.Console({
            handleExceptions: true
        })
    ],
    exitOnError: false
});

module.exports = logger;
