const helper = {
    readable: function(season) {
        return season.substring(0,4) + "-" +
        season.substring(0,2) + season.substring(4,6);
    },
    accessible: function(season) {
        if (season.length != 9) {
            return "";
        } 
        return season.substring(0,4)+season.substring(7,9);
        
    },
    ip: function (req) {
        return req.headers['x-forwarded-for']?.split(',').shift() 
        || req.socket?.remoteAddress;
    }
};

module.exports = helper;
