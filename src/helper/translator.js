/**
 * We don't have a table to map Div with suitable League name.
 * That's the reason of having this mapper object (for our case).
 * */
const mapper = {
    'SP1': 'La Liga Primera',
    'SP2': 'La Liga Segunda',
    'D1': 'Bundesligas',
    'E0': 'Premiership',
};

module.exports = function(div) {
    return mapper[div];
};
