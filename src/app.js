const logger = require('./logger');
const express = require('express');
const config = require('./config');
const redis = require('./redis');
const routers = require('./routers')(redis);

function logRequest(req, res, next) {
    logger.info(req.url);
    next();
}
function logError(err, req, res, next) {
    logger.error(err);
    next();
}
function startServer() {
    const app = express();
    app.use(logRequest);
    app.use(logError);
    app.use(routers);
    app.listen(config.http_port,() => {
        logger.info(`Http server started on ${config.http_port}`);
        app.get("/", (req, res) => {
            res.status(403).end();
        });
    }).on('error', err => {
        logger.error(err);
        process.exit(1);
    });
}

startServer();
