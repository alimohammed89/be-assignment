const dotenv = require('dotenv');
dotenv.config({path: `../.env.${process.env.NODE_ENV}`});
const config = {
    mysql_host: typeof(process.env.DB_HOST) === "undefined" ? "127.0.0.1" : process.env.DB_HOST,
    mysql_port: parseInt(process.env.DB_PORT, 10),
    mysql_username: process.env.DB_USERNAME,
    mysql_password: process.env.DB_PASSWORD,
    mysql_dbname: process.env.DB_NAME,
    http_port: parseInt(process.env.HTTP_PORT, 10),
    use_redis: process.env.USE_REDIS == 'true',
    redis_host: typeof(process.env.REDIS_HOST) === "undefined" ? "127.0.0.1" : process.env.REDIS_HOST
};
module.exports = config;
