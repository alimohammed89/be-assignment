module.exports = function(event) {
    /**
     * We can use Amplitude here instead of simple console.log method
     */
    console.log({event: event});
}
