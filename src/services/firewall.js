/**
 * Simple static ApiKey.
 */
const apiKey = "abcdefg123456";
exports.check = function(header) {
    return new Promise(function(resolve, reject) {
        if (header !== apiKey) {
            reject();
        } else {
            resolve();
        }
    });
    
};
