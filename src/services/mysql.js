const mysql = require('mysql');
const config = require('../config');
module.exports = {
    mysql_pool : mysql.createPool({
        host     : config.mysql_host,
        user     : config.mysql_username,
        password : config.mysql_password,
        database : config.mysql_dbname,
        port: 3306
    })
};
