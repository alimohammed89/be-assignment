# Football League and seasons API

This is a simple HTTP API that can do:

- HTTP GET Request to get all pairs.
- HTTP GET Request to get all games under that season.

## Features

- Easy to use and secured.
- You can run it locally or via docker-compose.

## Tech

This HTTP API has been developed using:

- [NodeJS] - As Backend service.
- [MySQL] - As Database engine.
- [ExpressJS] as HTTP service.
- [Redis] For list caching (As example of use only).

## Running

**Note that to run it locally you need to have a MySQL server and modify ```.env.development```**
```sh
cd src
npm i
node app
```
or
```sh
cd src
make up
make install
make test
make run
```

## Usage

**Please make sure that you'll send a custom-header called ```graceToken``` with ```abcdefg123456``` as value.**
- To get seaons / league pairs you can call: 
- ```localhost:3000/list```

- To get details for specific seasons call:
- ``` localhost:3000/league ```

## Accepted params for details request:

| Param | Required | Possible value(s) | Description
| ------ | ------ | ------ | ------ |
| season | YES | 2015-2016 | Season from previous list |
| div | YES | SP1 | Div from previous list |
| sort | NO | ID, Div, HomeTeam, AwayTeam, Date | Sort by |
| sorttype | NO | asc, desc | asending or desending |
| offset | NO | Default: 0 | Number of offset results in http request |


## Response

For the **list** request the response will be:
```sh
[{
    "title": "La Liga Primera 2015-2016",
    "results": "http://domain/league?someParams"
 }]
```
---
For the **details** request the response will be:
```sh
{
  "results": [
    {
      "ID": 771,
      "Div": "SP2",
      "Season": 201617,
      "Date": "2016-08-21T21:00:00.000Z",
      "HomeTeam": "Zaragoza",
      "AwayTeam": "UCAM Murcia",
      "FTHG": 3,
      "FTAG": 1,
      "FTR": "H",
      "HTHG": 3,
      "HTAG": 0,
      "HTR": "H\r"
    },
    {
      "ID": 793,
      "Div": "SP2",
      "Season": 201617,
      "Date": "2016-09-03T21:00:00.000Z",
      "HomeTeam": "Zaragoza",
      "AwayTeam": "Huesca",
      "FTHG": 1,
      "FTAG": 0,
      "FTR": "H",
      "HTHG": 0,
      "HTAG": 0,
      "HTR": "D\r"
    }],
    "resultsPerRequest": 20,
    "totalCount": 100
}
```

## Docker

This HTTP API is very easy to install and deploy in a Docker container.

By default, the Docker will expose port 3000, from the root folder of the project run

```sh
docker-compose up
```

Note that you'll need to **migrate** the DB to get results by running this command from root folder while ```docker-compose up``` is running:
```sh
cat src/db/migration/grace.dump | docker exec -i be-assignment_mysql_1 /usr/bin/mysql -u root --password=mysqlroot grace
```

